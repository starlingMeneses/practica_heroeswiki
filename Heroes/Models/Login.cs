﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Heroes.Models
{
    public class Login
    {
        [Required]
        [EmailAddress]
        [StringLength(50)]
        [Display(Name = "Correo Electronico")]
        public string email { set; get; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength = 8)]
        [Display(Name = "Contraseña")]
        public string password { set; get; }

        [Display(Name = "Recordar mi cuenta")]
        public bool recordar { set; get; }
    }
}
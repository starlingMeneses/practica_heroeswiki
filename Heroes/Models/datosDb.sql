﻿use [master]
go
if exists(select name from dbo.sysdatabases where name = 'DB_Heroes')
drop database [DB_Heroes]
GO
create  database [DB_Heroes]
go
use [DB_Heroes]
go

create table Tb_Usuarios(
idUsuario int identity primary key,
userName  varchar(max) not null,
password  varchar(100) not null,
email varchar(max),
fechaRegistro datetime not null default getdate())

go


create table Tb_Bus_Usuarios(
idBusqueda int identity primary key,
idUsuario int,
busqueda varchar (100),
userName varchar (100),
fechaBusqueda datetime not null default getdate(),
foreign key (idUsuario) REFERENCES Tb_Usuarios(idUsuario))
go


begin tran;
insert into Tb_Usuarios(userName,email,password)values( 'ALEXIS CORONADO ','alexis@gmail.com','1234');
insert into Tb_Usuarios(userName,email,password)values( 'ALVARO BONILLA ','alvaro@gmail.com','4321');
insert into Tb_Usuarios(userName,email,password)values( 'SANDRA TREJOS ','sadra@gmail.com','1111');
commit  tran;
go



CREATE procedure [dbo].[Sp_ValidarPerfil](
@email varchar(100),
@password varchar(max))
as
select email, userName, idUsuario
from  Tb_Usuarios
where rtrim(ltrim(email)) = rtrim(ltrim(@email))
and rtrim(ltrim(password)) = rtrim(ltrim(@password))
go

CREATE procedure [dbo].[Sp_InsertarUsuarios](
@email varchar(100),
@userName varchar(100),
@password varchar(100))
as
Insert into Tb_Usuarios(userName,password,email)
values(@userName,@password,@email)
go

CREATE procedure [dbo].[Sp_InsertarBusquedas](
@idUsuario varchar(100),
@Busqueda varchar(100),
@userName varchar(100))
as
Insert into Tb_Bus_Usuarios(idUsuario,busqueda,UserName)
values(@idUsuario,@Busqueda,@userName)
go


CREATE procedure [dbo].[Sp_GetSearches](
@idUsuario varchar(100))
as
select busqueda, idBusqueda
from  Tb_Bus_Usuarios
where rtrim(ltrim(idUsuario)) = rtrim(ltrim(@idUsuario))
go



select * from tb_usuarios




﻿using Heroes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Heroes.Models
{
   
    public class Powerstats
    {
        public string intelligence { get; set; }
        public string strength { get; set; }
        public string speed { get; set; }
        public string durability { get; set; }
        public string power { get; set; }
        public string combat { get; set; }
    }



    public class Appearance
    {
        public string gender { get; set; }
        public string race { get; set; }
        public List<string> height { get; set; }
        public List<string> weight { get; set; }
        
    }

    public class Work
    {
        public string occupation { get; set; }
        public string @base { get; set; }
    }


    public class Connections
    {
     
    public string relatives { get; set; }
}


public class Image
    {
        public string url { get; set; }
    }

    public class Result
    {
        public string id { get; set; }
        public string name { get; set; }
        public Powerstats powerstats { get; set; }

        public Appearance appearance { get; set; }
        public Work work { get; set; }
        public Image image { get; set; }

        public Connections connections { get; set; }
}

    public class ResultsList
    {
        public string response { get; set; }

        public List<Result> results { get; set; }

        public string busqueda { get; set; }

    }

}

     

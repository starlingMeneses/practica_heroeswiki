﻿using Heroes.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Heroes.Controllers
{
    public class HomeController : Controller
    {
        const string apikey = "2428484604129717";
        private const string ViewsUrl = "~/Views/Home/";




        [HttpPost]
        public PartialViewResult SearchData(string key)
        {
            var results = new ResultsList();


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"https://www.superheroapi.com/api.php/2428484604129717/search/" + key);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                json.Trim();
                results = JsonConvert.DeserializeObject<ResultsList>(json);
                var lol = results;

                results.busqueda = key;
                if (results != null)
                {
                    DB_HeroesEntities entidad = new DB_HeroesEntities();
                   
                    
                        entidad.Sp_InsertarBusquedas((Session["UserId"]).ToString(), key, (Session["UserPerfil"]).ToString());
                        entidad.SaveChanges();
                }

                return PartialView(ViewsUrl + "_Grid.cshtml", results);

            }
        }
        [HttpPost]
        public PartialViewResult LoadDataById(int id)
        {
            var results = new Result();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"https://www.superheroapi.com/api.php/2428484604129717/" + id);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                json.Trim();
                results = JsonConvert.DeserializeObject<Result>(json);
               

                return PartialView(ViewsUrl + "_Details.cshtml", results);
            }


        }

        [HttpPost]
        public PartialViewResult GetBusqueda()
        {

            DB_HeroesEntities entidad = new DB_HeroesEntities();
           
            int idUser = Convert.ToInt32(Session["UserId"]);
            var search = entidad.Tb_Bus_Usuarios
                .Where(u => u.idUsuario == idUser)
                .Distinct().ToList();

            return PartialView(ViewsUrl + "_Grid.cshtml", search);
        }

        public ActionResult Index()
        {
            if (Session["UserId"] == null) { return RedirectToAction("login", "user"); }
            else
            {

                DB_HeroesEntities entidad = new DB_HeroesEntities();
                
                int idUser = Convert.ToInt32(Session["UserId"]);
                var search = entidad.Tb_Bus_Usuarios
                    .Where(u => u.idUsuario == idUser)
                    .OrderByDescending(u => u.idBusqueda)
                    .Distinct().ToList();
                return View(ViewsUrl + "Index.cshtml", search);
            }
        }
    }
}
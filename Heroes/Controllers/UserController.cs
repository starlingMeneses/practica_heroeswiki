﻿using Heroes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Heroes.Controllers
{
    public class UserController : Controller
    {
        // GET: User

        User user = new User();
        public ActionResult Index()
        {
          
            return View();
        }

        [HttpGet]
        public ActionResult login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult login(Login login)
        {
            
            try
            {
                //se verifica las credenciales
                DB_HeroesEntities entidad = new DB_HeroesEntities();

                //se busca por el email el usuario corespondiente.
                Tb_Usuarios usuarios = entidad.Tb_Usuarios.FirstOrDefault(u => u.email == login.email);

                if (this.verificarUsuario(login.email, login.password))
                {
                    FormsAuthentication.SetAuthCookie(usuarios.email, login.recordar);
                   
                    Session["UserEmail"] = login.email;
                  

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Message = "NotAut";
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = ex.Message;
            }
            return View();
        }

        private bool verificarUsuario(string email, string password)
        {
            bool autorizado = false;

            //se instancia las entidades
            DB_HeroesEntities entidad = new DB_HeroesEntities();

            //se busca por el email el usuario corespondiente 
            Tb_Usuarios usuarios = entidad.Tb_Usuarios.FirstOrDefault(u => u.email == email);

            //se verifica si existe
            if (usuarios != null)
            {
                if (usuarios.password.Equals(password))
                {
                    autorizado = true;
                    Session["UserPerfil"] = usuarios.userName;
                    Session["UserId"] = usuarios.idUsuario;

                    user.id = usuarios.idUsuario;
                   
                }
            }

            return autorizado;
        }


        [HttpGet]
        public ActionResult register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult register(Tb_Usuarios usuario)
        {

            if (usuario.email.ToString().Length > 3 && usuario.userName.ToString().Length > 3 && usuario.password.ToString().Length > 3)
            {
                try
                {


                    //se instancia las entidades
                    DB_HeroesEntities entidad = new DB_HeroesEntities();

                    //se busca por el email el usuario corespondiente 
                    Tb_Usuarios usuarios = entidad.Tb_Usuarios.FirstOrDefault(u => u.email == usuario.email);

                    //se verifica si existe
                    if (usuarios != null)
                    {
                        ViewBag.Message = "Exists";
                    }
                    else
                    {
                        entidad.Sp_InsertarUsuarios(usuario.email, usuario.userName, usuario.password);
                        entidad.SaveChanges();

                        return RedirectToAction("login", "user");

                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = ex.Message;
                }
            }
            return RedirectToAction("register", "user");
        }
        [HttpGet]
        public ActionResult logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }

}